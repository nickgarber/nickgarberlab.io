#+TITLE: Emacs Init - Start-Up Configuration for GitLab-CI
#+PROPERTY: header-args :eval yes
#+PROPERTY: header-args :tangle no
#+PROPERTY: header-args :export no
#+PROPERTY: header-args :cache yes


* chapters
** Introduction
** Preparations
*** setup package manager
**** install, init, and require =straight.el=
  - bootstrap
    #+begin_src emacs-lisp
      (defvar bootstrap-version)
      (let ((bootstrap-file
	     (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	    (bootstrap-version 5))
	(unless (file-exists-p bootstrap-file)
	  (with-current-buffer
	      (url-retrieve-synchronously
	       "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	       'silent 'inhibit-cookies)
	    (goto-char (point-max))
	    (eval-print-last-sexp)))
	(load bootstrap-file nil 'nomessage))
    #+end_src
  - install =use-package=
    #+begin_src emacs-lisp
    (straight-use-package 'use-package)
    #+end_src
  - set =straight= as the default backed for =use-package=
    #+begin_src emacs-lisp
    (setq straight-use-package-by-default 't)
    #+end_src
*** install ox-hugo 
 - =use-package= will install =ox-hugo=
   #+begin_src emacs-lisp
     (use-package ox-hugo
       :after ox
       :config
       ;; https://ox-hugo.scripter.co/doc/auto-export-on-saving/
       ;; If you want the auto-exporting on file saves
       (require 'ox-hugo-auto-export))
   #+end_src
** Publish
 - publish
   #+begin_src emacs-lisp
   
   #+end_src
** Conclusion
*** COMMENT notes and references
  - context:
    - [0/0] overview
    - info/notes
      - srcs
        - https://ox-hugo.scripter.co/
        - https://gitlab.com/pages/hugo
        - examples of org-mode exported sites
	  - https://zzamboni.org/about/ ,, https://github.com/zzamboni/zzamboni.org
	  - https://gjhenrique.com/meta.html
	- examples of hugo sites using gitlab pages
          - https://blog.oscarnajera.com/2017/11/moving-to-hugo-and-gitlab-pages/
	- https://pages.gitlab.io/org-mode/ ,, https://gitlab.com/pages/org-mode
	  - https://gitlab.com/pages/org-mode/blob/master/.gitlab-ci.yml
	    #+begin_example
	      image: iquiw/alpine-emacs

	      .build: &build
		script:
		- emacs --batch --no-init-file --load publish.el --funcall org-publish-all
		artifacts:
		  paths:
		  - public

	      pages:
		<<: *build
		only:
		- master

	      test:
		<<: *build
		except:
		- master

	    #+end_example
	- https://emacs.stackexchange.com/questions/27126/is-it-possible-to-org-bable-tangle-an-org-file-from-the-command-line
	  #+CAPTION: loading org-babel code from a 'batch' instance of Emacs at the CLI 
	  #+begin_example
	  emacs --batch -l org foo.org -f org-babel-tangle
	  ## alternatively, to pass the file to load
	  emacs --batch -l org --eval "(org-babel-tangle-file \"$1\")"
	  #+end_example
    - Questions
      - Q: to use ox-hugo on gitlab pages would I need docker images for both emacs and hugo
	- https://docs.gitlab.com/ee//ci/docker/using_docker_images.html#what-services-are-not-for
	  #+begin_example
	    If you need to have php, node and go available for your script, you should either:

		,* choose an existing Docker image that contains all required tools, or
		,* create your own Docker image, which will have all the required tools included and use that in your job

	  #+end_example

	  - This snippet suggests to me that multiple images can't be used.

	- [2018-09-20 Thu 21:36] NGa muses I may have to create my own docker image to combine emacs and hugo, /or/ maybe I can install them using the package manage in base alpine
    - log
